package zeljko;

import java.util.HashSet;
import java.util.Set;



import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import zeljko.entities.repositories.Course;
import zeljko.entities.repositories.CourseRepository;
import zeljko.entities.repositories.Student;
import zeljko.entities.repositories.StudentRepository;
import zeljko.entities.repositories.User;
import zeljko.entities.repositories.UserRepository;

@SpringBootApplication
public class CrudbootApplication {
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(CrudbootApplication.class, args);
	}
	
	/**
	 * Save users and students to H2 DB for testing
	 * @param repository
	 * @return
	 */
	@Bean
	public CommandLineRunner demo(StudentRepository repository, CourseRepository crepository, UserRepository urepository) {
		return (args) -> {
			// save students
			Student student1 = new Student("Petar", "Petrovic", "IT", "petar@gmail.com"); 
			repository.save(new Student("Marija", "Maric", "Energetika", "marija@gmail.com"));
			repository.save(new Student("Marko", "Markovic", "IT", "marko@yahoo.com"));
			repository.save(new Student("Ivana", "Ivanovic", "Automatika","ivana@yahoo.com"));
			repository.save(new Student("Jelena", "Jovanovic", "Telekomunikacije","jelena@gmail.com"));
			
			Course course1 = new Course("Java Programiranje");
			Course course2 = new Course("Spring Boot");
			crepository.save(new Course("Elektronika"));
			crepository.save(new Course("Marketing"));
			
			crepository.save(course1);
			crepository.save(course2);
			
			Set<Course> courses = new HashSet<Course>();
			courses.add(course1);
			courses.add(course2);
			
			student1.setCourses(courses); 
			repository.save(student1);

			// Create users with BCrypt encoded password (user/user, admin/admin)
			User user1 = new User("user", "$2y$12$kCggr../b8cZN78ATUo6dO33kk6S3D3NW/tm64bYj2cO356ePK.ge", "USER");
			User user2 = new User("admin", "$2y$12$VY8kF84Ubr/LnEdCgHTOR.BcZ0CrWV4.YDTmV.oe.Kak0rV.sIyhy", "ADMIN");
			urepository.save(user1);
			urepository.save(user2); 
		};
	}
}
